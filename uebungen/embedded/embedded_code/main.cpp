#include "CMSIS/Device/ST/STM32L4xx/Include/stm32l4xx.h"

enum class ENPort : uint8_t {
    A = 0,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    MAX_PORT_NUMBER = H,
};

enum class ENMode : uint8_t {
    input = 0,
    output,
    alternate,
    analog,
};


enum class ENSpeed : uint8_t {
    low = 0,
    medium,
    high,
    veryHigh,
};


enum class ENPull : uint8_t {
    noPull = 0,
    pullUp,
    pullDown,
};


enum class ENOutMode : uint8_t {
    pushPull = 0,
    openDrain,
};

struct STGpioBase {
    ENPort port;
    uint8_t pin;
};

// Setup of a input pin
struct STGpioInputSetup {
    STGpioBase base;
    ENSpeed speed;
};

// Setup of a output pin
struct STGpioOutputSetup {
    STGpioBase base;
    ENSpeed speed;
    ENOutMode outMode;
    bool initialLevel;
};

GPIO_TypeDef *const GPIO_BASE_ADDR_LIST[] = {GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG, GPIOH};

void SetPinLevelInternal(GPIO_TypeDef *baseAddr, uint8_t pin, bool level) {
    uint8_t pinVal = pin;

    if (!level) {
        pinVal += 16;  // upper half are reset bits, lower half are set bits
    }
    SET_BIT(baseAddr->BSRR, 1u << pinVal);
}

void SetupSpeed(GPIO_TypeDef *baseAddr, uint8_t pin, ENSpeed speed) {
    const uint32_t speedVal = static_cast<uint32_t>(speed);

    // mask first field gives also the size and factor to address the other fields
    constexpr uint32_t OSPEEDR_BIT_FIELD_MASK = GPIO_OSPEEDR_OSPEED0_Msk >> GPIO_OSPEEDR_OSPEED0_Pos;
    const uint32_t speedBitShift = pin * GPIO_OSPEEDR_OSPEED1_Pos;

    MODIFY_REG(baseAddr->OSPEEDR, OSPEEDR_BIT_FIELD_MASK << speedBitShift,
               (speedVal & OSPEEDR_BIT_FIELD_MASK) << speedBitShift);
}

void SetupMode(GPIO_TypeDef *baseAddr, uint8_t pin, ENMode mode) {
    const uint32_t modeVal = static_cast<uint32_t>(mode);

    // mask first field gives also the size and factor to address the other fields
    constexpr uint32_t MODER_BIT_FIELD_MASK = GPIO_MODER_MODE0_Msk >> GPIO_MODER_MODE0_Pos;
    uint32_t moderBitShift = pin * GPIO_MODER_MODE1_Pos;

    MODIFY_REG(baseAddr->MODER, MODER_BIT_FIELD_MASK << moderBitShift,
               (modeVal & MODER_BIT_FIELD_MASK) << moderBitShift);
}

static void *GetBaseAddr(const ENPort port) {
    return GPIO_BASE_ADDR_LIST[static_cast<uint8_t>(port)];
}

void Setup(const STGpioInputSetup &s) {
    GPIO_TypeDef *baseAddr = static_cast<GPIO_TypeDef *>(GetBaseAddr(s.base.port));

    SetupSpeed(baseAddr, s.base.pin, s.speed);
    SetupMode(baseAddr, s.base.pin, ENMode::input);
}

void Setup(const STGpioOutputSetup &s) {
    GPIO_TypeDef *baseAddr = static_cast<GPIO_TypeDef *>(GetBaseAddr(s.base.port));

    SetupSpeed(baseAddr, s.base.pin, s.speed);

    const uint32_t outModeVal = static_cast<uint32_t>(s.outMode);
    MODIFY_REG(baseAddr->OTYPER, 0b1u << s.base.pin, (outModeVal & 0b1u) << s.base.pin);

    SetPinLevelInternal(baseAddr, s.base.pin, s.initialLevel);
    SetupMode(baseAddr, s.base.pin, ENMode::output);
}

void Gpio::Setup(const STGpioAlternateSetup & s)
{
    GPIO_TypeDef * baseAddr = static_cast<GPIO_TypeDef *>(GetBaseAddr(s.base.port));
    ASSERT_ALWAYS(s.base.pin < 16);

    SetupSpeed(baseAddr, s.base.pin, s.speed);

    // mask first field gives also the size and factor to address the other fields
    constexpr uint32 AFR_BIT_FIELD_MASK = GPIO_AFRL_AFSEL0_Msk >> GPIO_AFRL_AFSEL0_Pos;
    const uint32 afrBitShift            = (s.base.pin & 0b111u) * GPIO_AFRL_AFSEL1_Pos;

    /// AFRL will be used for channel index 0 to 7. and AFRH will be used for channel 8 to 15
    volatile uint32 & afrReg = baseAddr->AFR[s.base.pin <= 7u ? 0 : 1];
    MODIFY_REG(afrReg, AFR_BIT_FIELD_MASK << afrBitShift, (s.alternateFns & AFR_BIT_FIELD_MASK) << afrBitShift);

    SetupMode(baseAddr, s.base.pin, ENMode::alternate);
}

void SetPinLevel(const ENPort port,
                 const uint8_t pin,
                 const bool level
) {
    GPIO_TypeDef *baseAddr = static_cast<GPIO_TypeDef *>(GetBaseAddr(port));

    SetPinLevelInternal(baseAddr, pin, level);
}

int main(void) {
    while (1) {
    }
}
